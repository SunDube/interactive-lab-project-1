﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    private GameObject boss;

    void Start()
    {
        boss = GameObject.FindGameObjectWithTag("boss");
    }
    void OnTriggerEnter2D()
    {
        //if the cheese bottle is out of bounds the boss gets mad
        boss.GetComponent<SpriteChanger>().isHappy = false;
        Debug.Log("The Boss is Angry");
    }

    void OnTriggerExit2D()
    {

        //when the cheese bottle is in bounds boss goes back to normal
        boss.GetComponent<SpriteChanger>().isHappy = true;
        Debug.Log("The Boss is Angry");
    }
}
