﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseInstantiator : MonoBehaviour
{
    public GameObject cheeseParticle;
    // Start is called before the first frame update
    private int cheeseCount;

    // Update is called once per frame
    void Update()
    {
        //get key down so that cheese is only instantiated once per key press
        if (Input.GetKeyDown("a"))
        {
            MakeCheese();
        }

        if (Input.GetKeyDown("d"))
        {
            MakeCheese();
        }

        customerSatifaction();
    }

    void MakeCheese() {

        //how many cheese particles should be spawned
        int amountOfCheese = Random.Range(3, 7);
        cheeseCount = cheeseCount + amountOfCheese;

        for (int i = 0; i < amountOfCheese; i++)
        {
            Instantiate(cheeseParticle, this.transform.position, Quaternion.identity);
        }


    }

    void customerSatifaction() {

        GameObject customer = GameObject.FindGameObjectWithTag("customer");
        

        //customer has no cheese
        if (cheeseCount == 0)
        {
            Debug.Log("There is no Cheese!");
            customer.GetComponent<SpriteChanger>().isHappy = false;
        }


        //customer has enough cheese
        if (cheeseCount <= 100) {
            Debug.Log("Customer is Satisfied");
            customer.GetComponent<SpriteChanger>().isHappy = true;
        }

        //customer has too much cheese
        if (cheeseCount > 100)
        {
            Debug.Log("Customer is Not Pleased");
            customer.GetComponent<SpriteChanger>().isHappy = false;
        }

    }

    public void resetCounter() {
        cheeseCount = 0;
    }
}
