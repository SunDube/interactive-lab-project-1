﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacaroniMover : MonoBehaviour
{
    //The rigid body on the pasta plate
    private Rigidbody2D spaghettiStrings;

    //magnitude of force added to plate to discrd it - changable in inspector
    public float discardSpeed;
    //force added to plate to remove it
    private Vector2 discardPlate;
    //cheese shaker so that the counter can be reset
    private GameObject shaker;

    private bool freshPlate = true;

    

    
    void Awake()
    {
        spaghettiStrings = gameObject.GetComponent<Rigidbody2D>();

        //negative to make the plate move left
        discardPlate = new Vector2(-discardSpeed, 0);
        shaker = GameObject.FindGameObjectWithTag("shaker");
    }

    // Update is called once per frame
    void Update()
    {

        //throws the plate away when space is held down
        if (Input.GetKey("space"))
        {
            spaghettiStrings.AddForce(discardPlate);
        }

        //deletes the game object once its far below the camera
        if (transform.position.y < -25) {
            Destroy(this.gameObject);
        }

        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "cheese")
        {
            if (freshPlate == true)
            {
                
                if (shaker == null)
                {
                    Debug.Log("There is no shaker!");
                }
                else {
                    shaker.GetComponent<CheeseInstantiator>().resetCounter();
                }
                
                freshPlate = false;
            }
        }
    }
}
