﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour
{
    private SpriteRenderer smad;
    public Sprite happy;
    public Sprite mad;
    public bool isHappy = true;
    // Start is called before the first frame update
    void Start()
    {
        smad = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isHappy == true)
        {
            smad.sprite = happy;

        }
        else {

            smad.sprite = mad;

        }
    }
}
