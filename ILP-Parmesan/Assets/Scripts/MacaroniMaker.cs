﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacaroniMaker : MonoBehaviour
{
    public GameObject spaghetti;

    // Start is called before the first frame update
    void Start()
    {
        //create a plate of macaroni off screen when the game starts
        Instantiate(spaghetti, this.transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        //create another spaghetti when spacebar is released
        if (Input.GetKeyUp("space")) {

            Instantiate(spaghetti, this.transform.position, Quaternion.identity);

        }
    }
}
